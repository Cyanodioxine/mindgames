### MindGames ###

MindGames is a Turing complete, overcomplicated spinoff of Brainfuck. All Brainfuck programs are compatible.


### Introduction ###

Just like BF, MindGames is a language based off the tape system. You have a tape, which is an array of cells, and a pointer. You can move the pointer along the tape, and change the value at the point on the tape selected by the pointer.
However, that's not all it can do.

It was created to push the limit of a BF-like language, without breaking the fun. Because of the nature of the additions, there are now a million ways to complete the same tasks. That's the best part!

MindGames takes the original model of Brainfuck, and throws the restraints out the window. It supports:

* Subroutines

* For Loops

* Raw Manipulation (Store raw value, print raw value, etc.)

* Arithmetic (Addition, subtraction, multiplication, exponentiation out of the box)

* Lambdas

* Caching

* String Manipulation (Not quite done, but some good stuff is in there!)

* Number literals

* File Writing

* Comments

* Execute raw JavaScript

* More, coming soon...

### Breakdown ###

Token breakdown and examples coming soon.


### Contribution ###

Contributions are welcome, but can be denied. I'll provide reason for any denials, and credit for any accepted changes.

I'm not perfect. No programmer is. Let's work together to make this thing great.