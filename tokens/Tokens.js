var numbers = "0123456789";
var tokens = {};

function Token(token, handler) {
	this.token = token;
	this.execute = handler;
	this.parse = function(tokens) {};

	tokens[token] = this;
}

var tLeft = new Token('<', function(env, tokens, tape) {
	tape.prev();
});
tLeft.parse = function(tokens) {
	return {
		position: tokens.i,
		name: 'Move Left'
	}
}

var tRight = new Token('>', function(env, tokens, tape) {
	tape.next();
});
tRight.parse = function(tokens) {
	return {
		position: tokens.i,
		name: 'Move Right'
	}
}

var output = new Token('.', function(env, tokens, tape) {
	if(tape.get() == 10) {
		env.output += '\n';
	} else {
		env.output += String.fromCharCode(tape.get());
	}
});
output.parse = function(tokens) {
	return {
		position: tokens.i,
		name: 'Output'
	}
}

var cache = new Token('!', function(env, tokens, tape) {
	env.cache += String.fromCharCode(tape.get());
});
cache.parse = function(tokens) {
	return {
		name: 'Cache Value',
		position: tokens.i
	}
}

var incr = new Token('+', function(env, tokens, tape) {
	tape.incr();
});
incr.parse = function(tokens) {
	return {
		position: tokens.i,
		name: 'Increment 1'
	}
}

var decr = new Token('-', function(env, tokens, tape) {
	tape.decr();
});
decr.parse = function(tokens) {
	return {
		position: tokens.i,
		name: 'Decrement 1'
	}
}

var loopBegin = new Token('[', function(env, tokens, tape, debug) {
	if(tape.get() == 0) {
		if(debug) {
			var contents = getBracketContents(tokens, '[', ']', tokens.i);
			console.log(contents);
		}
		tokens.next();
		while(tape.loop > 0 || tokens.get() != ']') {
			if(tokens.get() == '[') tape.loop++;
			if(tokens.get() == ']') tape.loop--;
			tokens.next();
		}
	}
});
loopBegin.parse = function(tokens) {
	var pos = tokens.i;
	var loop = '';

	tokens.next();
	while(tokens.get() != ']') {
		loop += tokens.get();
		tokens.next();
	}
	tokens.prev();
	return {
		position: pos,
		name: 'Loop',
		token: '[',
		details: {
			contents: loop
		}
	};
}

var loopEnd = new Token(']', function(env, tokens, tape) {
	tokens.prev();
	while(tape.loop > 0 || tokens.get() != '[') {
		if(tokens.get() == ']') tape.loop++;
		if(tokens.get() == '[') tape.loop--;
		tokens.prev();
	}
	tokens.prev();
});
loopEnd.parse = function(tokens) {
	return null;
}


var cacheRaw = new Token('~', function(env, tokens, tape) {
	env.cache += tape.get();
});
cacheRaw.parse = function(tokens) {
	return {
		position: tokens.i,
		name: 'Cache Raw'
	}
}

var printRaw = new Token('=', function(env, tokens, tape) {
	env.output += tape.get();
});
printRaw.parse = function(tokens) {
	return {
		name: 'Print Raw',
		position: tokens.i
	}
}

var exp = new Token('^', function(env, tokens, tape) {
	var x = tape.get();
	while(tokens.get() == '^') {
		tape.mult(x);
		tokens.next();
	}
	tokens.prev();
});
exp.parse = function(tokens) {
	var out = {
		position: tokens.i,
		name: 'Exponentiate',
		token: '^'
	};
	var n = 1;
	while(tokens.get() == '^') {
		n++;
		tokens.next();
	}

	out.details = {
		expression: 'b^' + n,
		exponent: n
	};

	return out;
}

var mult = new Token('*', function(env, tokens, tape) {
	if(tape.i > 0) {
		tape.set(tape.i, tape.get() * tape.get(-1));
	}
});
mult.parse = function(tokens) {
	return {
		position: tokens.i,
		name: 'Multiply'
	}
}

var parentheses = new Token('(', function(env, tokens, tape) {
	tokens.next();

	var start = tokens.i;
	var end = getMatchingBracket(tokens, '(', ')', start);

	if(end != -1) {
		if(tokens.get() == 'x') {
			var expression = "";

			while(tokens.get() != "=") tokens.next();
			if(tokens.get(1) == ">") {
				tokens.jump(2);
				while(tokens.get() == ' ') tokens.next();
				while(tokens.i != start + end) {
					expression += tokens.get();
					tokens.next();
				}

				var code = expression + "; return x;";
				var func = Function.apply(null, ['x', 'tape', code]);
				var val = func(tape.get(), tape);
				tape.set(tape.i, val);
			}
		} else if(tokens.get() == '+' || tokens.get() == '-') {
			var val = tokens.get();
			tokens.next();

			while(numbers.includes(tokens.get())) {
				val += tokens.get();
				tokens.next();
			}

			if(tokens.get() == ')') {
				tape.incr(parseInt(val));
			}
		} else {
			var val = '';
			while(numbers.includes(tokens.get())) {
				val += tokens.get();
				tokens.next();
			}

			if(tokens.get() == ')') {
				tape.incr(parseInt(val));
			}
		}
	}
});
parentheses.parse = function(tokens) {
	var out = {
		position: tokens.i,
		name: '',
		token: '',
		details: ''
	}
	tokens.next();

	var start = tokens.i;
	var end = getMatchingBracket(tokens, '(', ')', start);

	if(end != -1) {
		if(tokens.get() == 'x') {
			out.name = 'Lambda'
			out.token = 'L';
			var expression = "";

			while(tokens.get() != "=") tokens.next();
			if(tokens.get(1) == ">") {
				tokens.jump(2);
				while(tokens.get() == ' ') tokens.next();
				while(tokens.i != start + end) {
					expression += tokens.get();
					tokens.next();
				}

				out.details = {
					expression: expression
				};
			}
		} else if(tokens.get() == '+' || tokens.get() == '-') {
			out.name = (tokens.get() == '+' ? 'Increment' : 'Decrement') + ' Raw';
			out.token = tokens.get() + 'I';
			var val = tokens.get();
			tokens.next();

			while(numbers.includes(tokens.get())) {
				val += tokens.get();
				tokens.next();
			}

			out.details = {
				value: val
			}
		} else {
			var val = '';
			while(numbers.includes(tokens.get())) {
				val += tokens.get();
				tokens.next();
			}

			out.name = 'Increment Raw';
			out.token = '+I';
			out.details = {
				value: '+' + val
			}
		}
	}
	return out;
}

var literal = new Token('"', function(env, tokens, tape) {
	var n = "";
	var i = tokens.i;

	tokens.next();

	try {
		while(tokens.get() != '"') {
			n += tokens.get();
			tokens.next();
		}
	} catch(e) {
		throw new Error('Cache Literal not closed at column ' + i);
	}

	env.cache += n.replace('\\n', '\r\n');
});
literal.parse = function(tokens) {
	var i = tokens.i;
	var n = "";
	tokens.next();

	try {
		while(tokens.get() != '"') {
			n += tokens.get();
			tokens.next();
		}
	} catch(e) {
		throw new Error('Cache Literal not closed at column ' + i);
	}

	return {
		name: 'Cache Literal',
		position: i,
		details: {
			contents: n
		}
	}
}

var que = new Token('@', function(env, tokens, tape) {
	tokens.next();
	var key = tokens.get();

	switch(key) {
		case '(': { //JavaScript
			tokens.next();
			var script = "";
			var start = tokens.i;
			var ending = getMatchingBracket(tokens, '(', ')', tokens.i);

			while(tokens.i != start + ending) {
				script += tokens.get();
				tokens.next();
			}

			Function.apply(null, ['env', 'tape', script])(env, tape);
		} break;
	}

	if(numbers.includes(key)) { //Subroutine
		var num = '';
		while(numbers.includes(tokens.get())) {
			num += tokens.get();
			tokens.next();
		}

		if(tokens.get() == '{') {
			tokens.next();
			var start = tokens.i;
			var end = getMatchingBracket(tokens, '{', '}', tokens.i);

			var subroutine = '';
			while(tokens.i != start + end) {
				subroutine += tokens.get();
				tokens.next();
			}

			env.defineSubroutine(num, subroutine);
		} else {
			env.executeSubroutine(num);
			tokens.prev();
		}
	}
});
que.parse = function(tokens) {
	var out = {
		position: tokens.i,
		name: '',
		token: '@',
		details: {}
	}

	tokens.next();
	var key = tokens.get();

	switch(key) {
		case '(': { //JavaScript
			out.name = 'Execute Raw'
			tokens.next();
			var script = "";
			var start = tokens.i;
			var ending = getMatchingBracket(tokens, '(', ')', tokens.i);

			while(tokens.i != start + ending) {
				script += tokens.get();
				tokens.next();
			}

			out.details.script = script;
		} break;
	}

	if(numbers.includes(key)) { //Subroutine
		var num = '';
		while(numbers.includes(tokens.get())) {
			num += tokens.get();
			tokens.next();
		}

		if(tokens.get() == '{') {
			tokens.next();
			var start = tokens.i;
			var end = getMatchingBracket(tokens, '{', '}', tokens.i);

			var subroutine = '';
			while(tokens.i != start + end) {
				subroutine += tokens.get();
				tokens.next();
			}

			out.name = 'Define Subroutine ' + num;
			out.details.routine = subroutine;
		} else {
			out.name = 'Execute Subroutine ' + num;
			delete out.details;
		}
	}

	return out;
}

var printCache = new Token(':', function(env, tokens, tape) {
	env.output += env.cache;
});
printCache.parse = function(tokens) {
	return {
		name: 'Print Cache',
		position: tokens.i
	}
}

var comment = new Token('#', function(env, tokens, tape) {
	while(tokens.get() == '#') tokens.next();
	while(tokens.get() != '#') tokens.next();
	while(tokens.get() == '#') tokens.next();
	tokens.prev();
});
comment.parse = function(tokens) {
	var cmmnt = '';
	var i = tokens.i;

	while(tokens.get() == '#') tokens.next();
	while(tokens.get() != '#') {
		cmmnt += tokens.get();
		tokens.next();
	}
	while(tokens.get() == '#') tokens.next();
	tokens.prev();

	return {
		name: 'Comment Block',
		position: i,
		details: {
			comment: cmmnt
		}
	}
}

var forLoop = new Token('/', function(env, tokens, tape) {
	tokens.next();
	var iter = "";
	var expression = "";

	while(tokens.get() != '/') {
		expression += tokens.get();
		tokens.next();
	}
	tokens.next();
	while(numbers.includes(tokens.get())) {
		iter += numbers[numbers.indexOf(tokens.get())];
		tokens.next();
	}
	iter = parseInt(iter);

	for(var i = 0; i < iter; i++) {
		env.exec(expression);
	}
	tokens.prev();
});
forLoop.parse = function(tokens) {
	var i = tokens.i;
	tokens.next();
	var iter = "";
	var expression = "";

	while(tokens.get() != '/') {
		expression += tokens.get();
		tokens.next();
	}
	tokens.next();
	while(numbers.includes(tokens.get())) {
		iter += numbers[numbers.indexOf(tokens.get())];
		tokens.next();
	}
	iter = parseInt(iter);
	tokens.prev();

	return {
		name: 'For Loop',
		position: i,
		details: {
			loops: iter,
			expression: expression
		}
	}
}

var filesystem = require('fs');

var amp = new Token('&', function(env, tokens, tape) {
	tokens.next();
	var node = tokens.get();

	switch(node) {
		case 'c': {
			env.reset();
		} break;
		case 'w': {
			filesystem.appendFileSync('log.txt', env.output, function () {});
		} break;
		case 'n': {
			filesystem.appendFileSync('log.txt', env.cache, function () {});
		} break;
		case 'r': {
			env.cache = '';
		} break;
		case 'f': {
			env.output = '';
		} break;
	}
});
amp.parse = function(tokens) {
	var out = {
		name: '',
		position: tokens.i
	}

	tokens.next();
	var node = tokens.get();

	switch(node) {
		case 'c': {
			out.name = 'Reset Environment';
		} break;
		case 'w': {
			out.name = 'Write Output';
		} break;
		case 'n': {
			out.name = 'Write Cache';
		} break;
		case 'r': {
			out.name = 'Clear Cache'
		} break;
		case 'f': {
			out.name = 'Clear Output'
		} break;
	}

	out.token = '&' + node;
	return out;
}


function getMatchingBracket(tokens, beginToken, endToken, start) {
	var tkns = tokens.slice(start);
	var count = 1;
	var i = 1;
	while(count > 0 && i < tkns.length) {
		var t = tkns[i];
		if(t == beginToken) count++;
		if(t == endToken) count--;

		if(count == 0) return i;

		i++;
	}
	return -1;
}

function getBracketContents(tokens, beginToken, endToken, start) {
	var tkns = tokens.slice(start);
	var count = 1;
	var i = 1;
	var contents = '';
	while(count > 0 && i < tkns.length) {
		var t = tkns[i];
		if(t == beginToken) {
			if(i != 1) contents += t;
			count++
		} else if(t == endToken) {
			if(count != 1) contents += t;
			count--
		} else {
			contents += t;
		}


		if(count == 0) contents;

		i++;
	}
	return '';
}

module.exports = tokens;
module.exports.getMatchingBracket = getMatchingBracket;
module.exports.getBracketContents = getBracketContents;
