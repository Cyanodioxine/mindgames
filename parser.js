var Tokens = require('./tokens/Tokens');

var subroutines = { };

var Runtime = function(tapesize) {
	this.size = tapesize || 100;
	this.ptr = 0;
	this.max = 1024;
	this.tape = this.createTape(this.size, this.max);
	this.cache = "";

	this.output = '';
}

Runtime.prototype.reset = function() {
	subroutines = {};
	this.ptr = 0;
	this.max = 1024;
	this.tape = this.createTape(this.size, this.max);
	this.cache = "";

	this.output = '';
}

Runtime.prototype.createTape = function(size, max) {
	var tape = new Array(size).fill(0);
	tape.maximum = max;
	tape.i = 0;
	tape.next = function() {this.i++; if(this.i == this.length) this.i = 0;};
	tape.prev = function() {this.i--; if(this.i < 0) this.i = this.length - 1};
	tape.get = function(offset) {if(offset) return this[this.i + offset]; return this[this.i]};
	tape.set = function(index, val) {this[index] = val};
	tape.incr = function(n) {if(n) { this[this.i] += n} else {this[this.i]++} if(this[this.i > this.maximum]) this.i = 0 };
	tape.decr = function() {if(this[this.i] > 0) this[this.i]--;};
	tape.mult = function(x) {this[this.i] *= x; this[this.i] = Math.max(Math.min(this[this.i], this.maximum), 0)};
	tape.loop = 0;

	return tape;
}

Runtime.prototype.tokenize = function(code) {
	var tokens = split(code);
	var out = [];

	while(!tokens.done()) {
		var token = tokens.get();
		if(Tokens[token] != null) {
			var o = Tokens[token].parse(tokens, this.tape);

			if(o != null) {
				if(!o.token) o.token = token;
				out.push(o);
			}
		}
		tokens.next();
	}
	return out;
}


Runtime.prototype.exec = function(code, debug) {
	if(typeof debug != 'boolean') debug = false;

	var tokens = split(code);

	while(!tokens.done()) {
		var token = tokens.get();
		if(Tokens[token] != null) {
			Tokens[token].execute(this, tokens, this.tape, debug);
		}
		tokens.next();
	}

	if(this.output.length !== 0 && this.output.trim()) {
		console.log(this.output);
	}
}

Runtime.prototype.executeSubroutine = function(id) {
	if(subroutines.hasOwnProperty(id)) {
		this.exec(subroutines[id]);
	}
}

Runtime.prototype.defineSubroutine = function(id, code) {
	if(!subroutines.hasOwnProperty(id)) {
		subroutines[id] = code;
	}
}

Runtime.prototype.getSubroutines = function() {
	return subroutines;
}

Runtime.prototype.getSubroutine = function(id) {
	if(subroutines.hasOwnProperty(id)) {
		return subroutines[id];
	}
	return null;
}

function split(code) {
	var tokens = code.split('');

	tokens.i = 0;
	tokens.next = function() {this.i++};
	tokens.prev = function() {this.i--};
	tokens.done = function() {return this.i == this.length};
	tokens.get = function(x) {if(x) return this[this.i + x]; return this[this.i]};
	tokens.jump = function(n) {this.i += n};

	return tokens;
}

module.exports = Runtime;