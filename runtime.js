process.stdin.setEncoding('utf8');

var Runtime = require('./parser');
var runtime = new Runtime(1024);

var readline = require('readline');
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
	prompt: '*> '
});

rl.prompt();

rl.on('line', (text) => {
	if(text.indexOf('`') == 0) {
		var code = text.substring(1);
		runtime.exec(text);
		console.log(runtime.tokenize(text));
	} else if(text.indexOf('?') == 0) {
		var code = text.substr(1);
		runtime.exec(code, true);
	} else {
		runtime.exec(text);
	}
	rl.prompt();
}).on('close', () => {
	process.exit(0);
});