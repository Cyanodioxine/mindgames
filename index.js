if (require.main === module) {
	module.exports = require('./runtime');
} else {
	module.exports = require('./parser');
}