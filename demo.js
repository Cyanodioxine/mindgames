var Runtime = require('./parser');
var interpreter = new Runtime(100); //100 cells

//Mostly Brainfuck compatible
//For Brainfuck to be fully implemented, need to handle newlines vs 10s, and over/underflow needs fixed.
//interpreter.exec('++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.'); //Hello World!
//interpreter.exec('#Anything surrounded in hash symbols will not be interpreted.#');

//The parser isn't yet done
//var tokens = interpreter.parse('@120{|x => x = 65|} /@120!>/11 /</11 ');
//console.log(JSON.stringify(tokens));

var Tokens = require('./tokens/Tokens');
var tokens = '+++++++(+++[>++()+()++++>)++++++++++>+++>+<<<<-]'.split('');
tokens.i = 0;
tokens.next = function() {this.i++};
tokens.prev = function() {this.i--};
tokens.done = function() {return this.i == this.length};
tokens.get = function(x) {if(x) return this[this.i + x]; return this[this.i]};
tokens.jump = function(n) {this.i += n};

console.log('Program length: ' + tokens.length);

for(var i = 0; i < tokens.length; i++) {
	if(tokens[i] == '(') {
		tokens.i = i + 1;
		break;
	}
}
var script = "";
var start = tokens.i;
var ending = Tokens.getMatchingBracket(tokens, '(', ')', tokens.i);
while(tokens.i != start + ending) {
	script += tokens.get();
	tokens.next();
}
console.log('Ending pos: ' + ending);
console.log('Script: ' + script);


//Here's a fancy way of doing the same thing, using "most" of the MindGames features (WIP, too lazy to finish right now)
// interpreter.exec('@120{|x => x = 65|}'); //Subroutine 120 fills the cell with 'A'
// interpreter.exec('/@120!>/11'); //Execute the subroutine for each cell, save value to cache
// interpreter.exec('/</11'); //Back to the beginning
// interpreter.exec('/+/'); //Set the first cell to 'H"
// interpreter.exec(':'); //Print the cache
// interpreter.exec('\q'); //Quit

//Or, here's a way to do the same thing as above!
//interpreter.exec('"Hello World!":'); //Anything surrounded in quotes gets inserted into the cache

//Or another way!
//Numbers surrounded by parentheses are added/subtracted to pointer (+32) and (32) adds 32, (-32) subtracts 32
//interpreter.exec('(72).>(101).>(108).>(108).>(111).>(32).>(87).>(111).>(114).>(108).>(100).>(33).(10).'); //Again, need to handle the newline vs 10s thing for Brainfuck